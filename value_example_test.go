package env_test

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/slxh/go/env"
)

func ExampleNewValue() {
	// EnvSet the environment variable.
	// This is usually done outside the program.
	os.Setenv("A", "15")

	// Create a new variable
	i, v := env.NewValue(-1)

	// Register and read from environment.
	// Note that you can use `env.Var` (and `env.Parse()`) directly as well.
	es := env.NewEnvSet("", env.ReturnFirstError)
	es.SetOutput(os.Stdout)
	es.Var(v, "A", "Initial count (`number`)")

	// Override with CLI flags.
	// Note that you can use `flag.Var` (and `flag.Parse()`) directly as well.
	fs := flag.NewFlagSet("test", flag.ExitOnError)
	fs.SetOutput(os.Stdout)
	fs.Var(v, "count", "Initial count (`number`)")

	// Parse. Flags will override environment variables.
	es.Parse()
	fs.Parse(nil)

	// Print UsageString of both
	es.PrintDefaults()
	fs.PrintDefaults()

	// Get value:
	fmt.Printf("Count: %v", *i)

	// Output:
	//    A number
	//     	Initial count (number) (default -1)
	//   -count number
	//     	Initial count (number) (default -1)
	// Count: 15
}
