package env_test

import (
	"bytes"
	"fmt"
	"testing"
	"time"

	"gitlab.com/slxh/go/env"
)

func TestAdd_Lookup(t *testing.T) {
	setup(t)

	val := -5
	name := "TEST"
	usage := "test me"

	v := env.NewValueVar(&val, val)
	env.Var(v, name, usage)
	e := env.Lookup(name)

	if e.Value != v {
		t.Errorf("Incorrect value: expected %#v, got %#v", v, e.Value)
	}

	if e.Name != name {
		t.Errorf("Incorrect name: expected %q, got %q", name, e.Name)
	}

	if e.Usage != usage {
		t.Errorf("Incorrect UsageString: expected %q, got %q", usage, e.Usage)
	}
}

func TestParse(t *testing.T) {
	setup(t)

	var i int

	t.Setenv("TEST_PARSE_VAR", "-123")
	env.SetPrefix("TEST_PARSE_")
	env.Var(env.NewValueVar(&i, 0), "VAR", "")
	env.Parse()

	if i != -123 {
		t.Errorf("Parse incorrect: expected %v, got %v", -123, i)
	}
}

func TestParseWithFlags(t *testing.T) {
	setup(t)

	var i int

	t.Setenv("VAR", "not a number")

	env.AddVar(&i, "VAR", 0, "")

	if err := env.ParseWithFlags(); err == nil {
		t.Error("Expected error, got nil")
	}
}

func TestParseWithFlags_usage(t *testing.T) {
	setup(t)
	t.Setenv("HELP", "")

	if err := env.ParseWithFlags(); err == nil || err.Error() != "env: help requested" {
		t.Error("Expected error, got nil")
	}
}

func TestPrintDefaults(t *testing.T) {
	setup(t)

	var buf bytes.Buffer

	env.DefaultSet.SetOutput(&buf)

	env.Add("A", 5, "A")
	env.Add("B", 0, "B")
	env.Add("C", "", "C")
	env.Add("D", "x", "D")

	env.PrintDefaults()

	exp := "  A int\n" +
		"    \tA (default 5)\n" +
		"  B int\n" +
		"    \tB\n" +
		"  C string\n" +
		"    \tC\n" +
		"  D string\n" +
		"    \tD (default \"x\")\n"
	if buf.String() != exp {
		t.Errorf("Expected %q, got %q", exp, buf.String())
	}
}

func TestSetErrorHandling(t *testing.T) {
	setup(t)
	t.Setenv("A", "not a number")
	t.Setenv("B", "1 jiffy")

	env.Add("A", 0, "A number")
	env.Add("B", time.Second, "A duration")

	expFirst := `cannot set A to "not a number": cannot parse int: ` +
		`strconv.ParseInt: parsing "not a number": invalid syntax`
	expLast := `cannot set B to "1 jiffy": cannot parse time.Duration: ` +
		`time: unknown unit " jiffy" in duration "1 jiffy"`
	expAll1 := expFirst + "\n" + expLast
	expAll2 := expLast + "\n" + expFirst

	env.SetErrorHandling(env.ReturnFirstError)

	if err := env.Parse(); err == nil || (err.Error() != expFirst && err.Error() != expLast) {
		t.Errorf("Incorrect error: %q, expected %q or %q", err, expFirst, expLast)
	}

	env.SetErrorHandling(env.ReturnLastError)

	if err := env.Parse(); err == nil || (err.Error() != expFirst && err.Error() != expLast) {
		t.Errorf("Incorrect error: %q, expected %q or %q", err, expLast, expFirst)
	}

	env.SetErrorHandling(env.ReturnAllErrors)

	if err := env.Parse(); err == nil || (err.Error() != expAll1 && err.Error() != expAll2) {
		t.Errorf("Incorrect error: %q, expected %q or %q", err, expAll1, expAll2)
	}
}

func TestUsage(t *testing.T) {
	setup(t)

	var (
		i int
		s string
	)

	env.Var(env.NewValueVar(&i, 0), "VAR1", "Read me.")
	env.Var(env.NewValueVar(&s, "x"), "VAR2", "Read me too.")

	exp := "  VAR1 int\n" +
		"    \tRead me.\n" +
		"  VAR2 string\n" +
		"    \tRead me too. (default \"x\")"
	if env.Usage() != exp {
		t.Errorf("Usage incorrect: expected %q, got %q", exp, env.Usage())
	}
}

func TestVisitAll(t *testing.T) {
	setup(t)

	t.Setenv("A", "-1")

	env.Add("A", 0, "A")
	env.Add("B", 0, "B")

	env.Parse()

	var got string

	env.VisitAll(func(e *env.Env) {
		got += fmt.Sprintf("%s=%s, ", e.Name, e.Value)
	})

	exp := "A=-1, B=0, "
	if exp != got {
		t.Errorf("Expected %q, got %q", exp, got)
	}
}
