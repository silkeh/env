package env_test

import (
	"flag"
	"os"

	"gitlab.com/slxh/go/env"
)

func ExampleEnvSet_Parse() {
	// Define an env set to show the help for.
	es := env.NewEnvSet("", env.ReturnAllErrors)
	es.SetOutput(os.Stdout)
	es.Add("I", env.NewValueVar(new(int), 1), "A number")

	// Set the help environment variable to show help
	os.Setenv("HELP", "")

	// Show usage
	es.Parse()

	// Unset the environment variable for other tests
	os.Unsetenv("HELP")

	// Output:
	// Usage:
	//   I int
	//     	A number (default 1)
}

func ExampleEnvSet_SetFlagUsage() {
	// Define a flag set to show the help for.
	fs := flag.NewFlagSet("", flag.ContinueOnError)
	fs.SetOutput(os.Stdout)
	fs.Int("a", 1, "A number")

	// Define an env set to show the help for.
	es := env.NewEnvSet("", env.ReturnAllErrors)
	es.SetOutput(os.Stdout)
	es.Add("B", env.NewValueVar(new(int), 2), "Another number")

	// Override usage message
	es.SetFlagUsage(fs)

	fs.Parse([]string{"-h"})
	// Output:
	// Usage:
	// Flags:
	//   -a int
	//     	A number (default 1)
	//
	// Environment variables:
	//   B int
	//     	Another number (default 2)
}
