package env_test

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/slxh/go/env"
)

func ExampleEnv_UsageString() {
	_, v := env.NewValue(5 * time.Second)
	a := env.Env{
		Name:  "A",
		Usage: "a `name` to show",
		Value: v,
	}
	b := env.Env{
		Name:  "B",
		Usage: "a description",
		Value: v,
	}

	fmt.Println(a.UsageString("PREFIX_"))
	fmt.Println(b.UsageString("PREFIX_"))

	// Output:
	//   PREFIX_A name
	//     	a name to show (default 5s)
	//   PREFIX_B duration
	//     	a description (default 5s)
}

func ExampleEnv_UnquoteUsage() {
	_, v := env.NewValue(5 * time.Second)
	e := env.Env{
		Name:  "DURATION",
		Usage: "a `name` to show",
		Value: v,
	}

	kind, usage := e.UnquoteUsage()
	fmt.Printf("%s, %s", kind, usage)

	// Output:
	// name, a name to show
}

func TestEnv_UsageString(t *testing.T) {
	e := env.Env{
		Name: "A",
	}
	exp := "  A nil\n    \t"

	if e.UsageString("") != exp {
		t.Errorf("Expected usage string %q, got %q", exp, e.UsageString(""))
	}
}

func ExampleEnv_UnquoteUsage_noBackticks() {
	_, v := env.NewValue(5 * time.Second)
	e := env.Env{
		Name:  "TIME_B",
		Usage: "a description without backticks",
		Value: v,
	}

	kind, usage := e.UnquoteUsage()
	fmt.Printf("%s, %s", kind, usage)

	// Output:
	// duration, a description without backticks
}

func TestEnv_UnquoteUsage(t *testing.T) {
	var (
		i  int
		ts time.Time
		d  time.Duration
	)

	tests := []struct {
		Env         env.Env
		Kind, Usage string
	}{
		{
			Env: env.Env{
				Usage: "a `name` to show",
			},
			Kind:  "name",
			Usage: "a name to show",
		},
		{
			Env: env.Env{
				Value: env.NewValueVar(&i, 15),
			},
			Kind: "int",
		},
		{
			Env: env.Env{
				Value: env.NewValueVar(&ts, time.Unix(0, 0)),
			},
			Kind: "timestamp",
		},
		{
			Env: env.Env{
				Value: env.NewValueVar(&d, 15*time.Hour),
			},
			Kind: "duration",
		},
		{
			Env:  env.Env{},
			Kind: "nil",
		},
	}

	for n, test := range tests {
		kind, usage := test.Env.UnquoteUsage()
		if kind != test.Kind {
			t.Errorf("%v: expected kind %q, but got %q", n, test.Kind, kind)
		}

		if usage != test.Usage {
			t.Errorf("%v: expected usage %q, but got %q", n, test.Usage, usage)
		}
	}
}
