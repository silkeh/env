package env_test

import (
	"flag"
	"os"
	"testing"
	"time"

	"gitlab.com/slxh/go/env"
)

func setup(tb testing.TB) {
	tb.Helper()

	reset()
	tb.Cleanup(reset)
}

func reset() {
	env.DefaultSet = env.NewEnvSet("", env.ReturnLastError)
}

func testEnv(tb testing.TB) *env.EnvSet {
	tb.Helper()

	return testEnvErr(tb, env.ReturnFirstError)
}

func testEnvErr(tb testing.TB, h env.ErrorHandling) *env.EnvSet {
	tb.Helper()
	setup(tb)

	s := env.NewEnvSet("TEST_", h)
	s.Add("A", env.NewValueVar(new(int), 0), "A number")
	s.Add("B", env.NewValueVar(new(time.Duration), 0), "A duration")

	return s
}

func TestEnvSet_Lookup(t *testing.T) {
	if e := testEnv(t).Lookup("not defined"); e != nil {
		t.Errorf("Expected an undefined variable to return nil, but got %#v", e)
	}
}

func TestEnvSet_Output(t *testing.T) {
	s := testEnv(t)
	if s.Output() != os.Stderr {
		t.Errorf("Expected Output to return Stderr, but got %#v", s.Output())
	}
}

func TestEnvSet_Parse_ErrorHandling(t *testing.T) {
	t.Setenv("TEST_A", "not a number")
	t.Setenv("TEST_B", "1 jiffy")

	expFirst := `cannot set A to "not a number": cannot parse int: ` +
		`strconv.ParseInt: parsing "not a number": invalid syntax`
	expLast := `cannot set B to "1 jiffy": cannot parse time.Duration: ` +
		`time: unknown unit " jiffy" in duration "1 jiffy"`
	expAll1 := expFirst + "\n" + expLast
	expAll2 := expLast + "\n" + expFirst

	err := testEnvErr(t, env.ReturnFirstError).Parse()
	if err == nil || (err.Error() != expFirst && err.Error() != expLast) {
		t.Errorf("Incorrect error: %q, expected %q or %q", err, expFirst, expLast)
	}

	err = testEnvErr(t, env.ReturnLastError).Parse()
	if err == nil || (err.Error() != expFirst && err.Error() != expLast) {
		t.Errorf("Incorrect error: %q, expected %q or %q", err, expLast, expFirst)
	}

	err = testEnvErr(t, env.ReturnAllErrors).Parse()
	if err == nil || (err.Error() != expAll1 && err.Error() != expAll2) {
		t.Errorf("Incorrect error: %q, expected %q or %q", err, expAll1, expAll2)
	}
}

func TestEnvSet_ParseWithFlagSet(t *testing.T) {
	fs := flag.NewFlagSet("test", flag.ContinueOnError)
	fs.Duration("test-a", 0, "")

	s := testEnv(t)
	if err := s.ParseWithFlagSet(fs, nil); err != nil {
		t.Errorf("Unexpected error: %s", err)
	}

	if err := s.ParseWithFlagSet(fs, []string{"-test-a=1j"}); err == nil {
		t.Errorf("Expected error, got none")
	}

	t.Setenv("TEST_A", "1 jiffy")

	if err := s.ParseWithFlagSet(fs, nil); err == nil {
		t.Errorf("Expected error, got none")
	}
}

func TestEnvSet_Set(t *testing.T) {
	if err := testEnv(t).Set("B", "1 jiffy"); err == nil {
		t.Errorf("Expected error, got none")
	}

	if err := testEnv(t).Set("does not exist", "1 jiffy"); err == nil {
		t.Errorf("Expected error, got none")
	}
}
