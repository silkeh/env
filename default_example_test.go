package env_test

import (
	"flag"
	"fmt"
	"os"
	"strings"

	"gitlab.com/slxh/go/env"
)

func ExampleAdd() {
	// Set the environment variable.
	// This is usually done outside the program.
	os.Setenv("A", "5")

	// Create and register the variable and initial value.
	i := env.Add("A", -1, "Initial count")
	env.Parse()

	// Show
	fmt.Printf("Count: %v", *i)

	// Output: Count: 5
}

func ExampleAddVar() {
	var i int

	// Set the environment variable.
	// This is usually done outside the program.
	os.Setenv("A", "5")

	// Register the variable and initial value.
	env.AddVar(&i, "A", -1, "Initial count")
	env.Parse()

	// Show
	fmt.Printf("Count: %v", i)

	// Output: Count: 5
}

func ExampleFunc() {
	var v string

	// This is usually done outside the program.
	os.Setenv("A", "Something or other.")

	// Register the function for the environment variable.
	env.Func("A", "Initial count", func(s string) error {
		v = strings.ToUpper(s)

		return nil
	})

	env.Parse()

	// Show
	fmt.Println(v)

	// Output: SOMETHING OR OTHER.
}

func ExampleParsed() {
	env.Parse()

	fmt.Println(env.Parsed())

	// Output: true
}

func ExampleParseWithFlags() {
	// Set the environment variable.
	// This is usually done outside the program.
	os.Setenv("A", "-1")

	// Define a CLI flag.
	i := flag.Int("A", 0, "Some flag")

	// Parse the flags as environment variables, and parse both flags and environment variables.
	env.ParseWithFlags()

	// Show
	fmt.Printf("Value: %v", *i)

	// Output: Value: -1
}

func ExampleSet() {
	// Set the environment variable.
	// This is usually done outside the program.
	os.Setenv("A", "-1")

	// Create and register the variable and initial value.
	i := env.Add("A", 0, "A")

	env.Parse()

	fmt.Println(*i)

	env.Set("A", "15")

	fmt.Println(*i)

	// Output:
	// -1
	// 15
}

func ExampleSetFlagUsage() {
	// Override args and flag set to make this example produce consistent output.
	// This is not needed outside of tests.
	os.Args = []string{"example_application", "-help"}
	env.DefaultSet = env.NewEnvSet("", env.ReturnFirstError)
	flag.CommandLine = flag.NewFlagSet(os.Args[0], flag.ContinueOnError)
	flag.CommandLine.SetOutput(os.Stdout)

	// Define flags and environment variables to show help for.
	flag.Int("a", 1, "A variable")
	env.Add("B", 2, "Another variable")

	// Override usage message
	env.SetFlagUsage(nil)

	flag.Parse()
	// Output:
	// Usage of example_application:
	// Flags:
	//   -a int
	//     	A variable (default 1)
	//
	// Environment variables:
	//   B int
	//     	Another variable (default 2)
}

func ExampleVisit() {
	// Set the environment variable.
	// This is usually done outside the program.
	os.Setenv("A", "-1")
	os.Setenv("B", "-10")

	// Create and register the variable and initial value.
	env.Add("A", 0, "A")
	env.Add("B", 0, "B")

	env.Parse()

	env.Visit(func(e *env.Env) {
		fmt.Printf("Flag %q has been set to %q\n", e.Name, e.Value)
	})

	// Output:
	// Flag "A" has been set to "-1"
	// Flag "B" has been set to "-10"
}
